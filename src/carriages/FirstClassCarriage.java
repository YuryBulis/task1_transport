package carriages;

public class FirstClassCarriage extends Carriage {
    public FirstClassCarriage() {
        super("First Class Carriage", 64);
    }
}
