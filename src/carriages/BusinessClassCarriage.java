package carriages;

public class BusinessClassCarriage extends Carriage{

    public BusinessClassCarriage() {
        super ("Business Class Carriage", 36);
    }
}
