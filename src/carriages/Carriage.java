package carriages;

public class Carriage {

    private String carriageType;
    private int passengerCapacity;
    private int luggageCapacity;



    Carriage(String carriageType, int passengerCapacity) {
        this.carriageType = carriageType;
        this.passengerCapacity = passengerCapacity;
        this.luggageCapacity = passengerCapacity*2;
    }

    public String getCarriageType() {
        return carriageType;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public int getLuggageCapacity() {
        return luggageCapacity;
    }
}
