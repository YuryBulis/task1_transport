package train;

import carriages.Carriage;

import java.util.ArrayList;

public class Train {

    private ArrayList<Carriage> carriages = new ArrayList<>();

    public void add(Carriage carriage) {
        carriages.add(carriage);
    }

    public void getTotalPassengers() {
        int result = 0;
        for (Carriage carriage : carriages) {
            result += carriage.getPassengerCapacity();
        }
        System.out.println("The total number of passengers is " + result);
    }

    public void getTotalLuggage() {
        int result = 0;
        for (Carriage carriage : carriages) {
            result += carriage.getLuggageCapacity();
        }
        System.out.println("The total amount of luggage is " + result);
    }

    public void getTotalCarriages() {
        System.out.println("The total number of carriages is " + carriages.size());
    }

    public void sortCarriageType() {
        int i = 0;
        int j = 0;
        int k = 0;
        for (Carriage carriage : carriages) {
            switch (carriage.getCarriageType()) {
                case "Business Class Carriage":
                    i++;
                    break;
                case "Econom Class Carriage":
                    j++;
                    break;
                default:
                    k++;
                    break;
            }
        }

        System.out.println("The total number of business class carriages is " + i);
        System.out.println("The total number of econom class carriages is " + j);
        System.out.println("The total number of first class carriages is " + k);
    }

    public void findEconomCarriage() {
        int i = 0;
        for (Carriage carriage : carriages) {
            i++;
            if (carriage.getCarriageType().equals("Econom Class Carriage")) {
                System.out.println("Carriage number " + i + " is Econom class");
            } else {
                System.out.println("There are no Econom class carriages");
            }
        }
    }
}
