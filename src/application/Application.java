package application;

import carriages.BusinessClassCarriage;
import carriages.EconomClassCarriage;
import carriages.FirstClassCarriage;
import train.Train;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        boolean isTrainCreated = false;
        Train train = null;
        boolean isExit = false;

        while (!isExit) {
            System.out.println("1 - Create a train \n" +
                    "2 - Add carriage to the train \n" +
                    "3 - Calculate the total number of passengers and luggage \n" +
                    "4 - Sort carriages by carriage type \n" +
                    "5 - Find econom class carriage \n" +
                    "6 - Exit the application");
            Scanner in = new Scanner(System.in);
            int choice = in.nextInt();

            switch (choice) {
                case 1:

                    if (isTrainCreated) {
                        System.out.println("You have already created a train");
                    } else {
                        train = new Train();
                        isTrainCreated = true;
                        System.out.println("Train is created");
                    }
                    break;

                case 2:
                    if (isTrainCreated) {
                        System.out.println("Enter the carriage type \n" +
                                "1 - Business clas \n" +
                                "2 - Econom class \n" +
                                "3 - First class");
                        int carriageChoice = in.nextInt();

                        if (carriageChoice == 1) {
                            BusinessClassCarriage businessClassCarriage = new BusinessClassCarriage();
                            train.add(businessClassCarriage);
                            System.out.println("Business class carriage is created");
                        } else if (carriageChoice == 2) {
                            System.out.println("Econom class carriage is created");
                            EconomClassCarriage economClassCarriage = new EconomClassCarriage();
                            train.add(economClassCarriage);
                        } else if (carriageChoice == 3) {
                            System.out.println("First class carriage is created");
                            FirstClassCarriage firstClassCarriage = new FirstClassCarriage();
                            train.add(firstClassCarriage);
                        } else {
                            System.out.println("Error. Try again");
                        }
                    } else {
                        System.out.println("First create train");
                    }
                    break;

                case 3:
                    if (isTrainCreated) {
                        train.getTotalPassengers();
                        train.getTotalLuggage();
                    } else {
                        System.out.println("First create train");
                    }
                    break;

                case 4:
                    if (isTrainCreated) {
                        train.getTotalCarriages();
                        train.sortCarriageType();
                    } else {
                        System.out.println("First create train");
                    }
                    break;

                case 5:
                    if (isTrainCreated) {
                        train.findEconomCarriage();
                    } else {
                        System.out.println("First create train");
                    }
                    break;

                case 6:
                    isExit = true;
                    System.out.println("Bye-Bye");
                    break;

                default:
                    System.out.println("Error. Try again");
                    break;
            }

        }

    }
}
